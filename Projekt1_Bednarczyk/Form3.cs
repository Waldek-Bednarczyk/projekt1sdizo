﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt1_Bednarczyk
{
    public partial class Form3 : Form
    {
        List list = new List();
        private int[] values;
        int a = 1000;
        public Form3(int[] data)
        {
            InitializeComponent();
            for(int i=0; i<data.Length; i++)
            {
                list.AddBack(data[i]);
            }
            string path = "C:\\Users\\walde\\Desktop\\data" + a + ".txt";
            using (StreamReader sr = new StreamReader(path))
            {
                string numbers = sr.ReadToEnd();
                string[] numbersSplitted = numbers.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);
                int numberOfValues = Convert.ToInt32(numbersSplitted[0]);
                values = new int[numberOfValues];
                for (int i = 0; i < numberOfValues; i++)
                {
                    values[i] = Convert.ToInt32(numbersSplitted[i + 1]);
                }
            }
        }

        private void buttonAddFront_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i=0; i<a; i++)
            {
                list.AddFront(values[i]);
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();

            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void textBoxValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && (e.KeyChar != '-');
        }

        private void textBoxIndex_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void buttonAddEnd_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i = 0; i < a; i++)
            {
                list.AddBack(values[i]);
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();

            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i = 0; i < a-1; i++)
            {
                list.checkIfExists(values[i]);
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();

            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();

        }

        private void buttonDeleteFirst_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i = 0; i < a; i++)
            {
                list.deleteFront();
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();

            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonDeleteback_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i = 0; i < a; i++)
            {
                list.deleteBack();
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();

            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonDeleteAtIndex_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i = 0; i < a - 11; i++)
            {
                list.deleteAtIndex(10);
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonAddAtIndex_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i = 0; i < a; i++)
            {
                list.AddAtIndex(values[i], i + 1);
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "Lista.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(list.counter + " " + list.displayList());
            sw.Close();
        }
    }
}
