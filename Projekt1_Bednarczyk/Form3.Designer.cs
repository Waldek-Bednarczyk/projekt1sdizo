﻿namespace Projekt1_Bednarczyk
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddFront = new System.Windows.Forms.Button();
            this.buttonAddEnd = new System.Windows.Forms.Button();
            this.buttonAddAtIndex = new System.Windows.Forms.Button();
            this.buttonDeleteFirst = new System.Windows.Forms.Button();
            this.buttonDeleteback = new System.Windows.Forms.Button();
            this.buttonDeleteAtIndex = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxIndex = new System.Windows.Forms.TextBox();
            this.textBoxValue = new System.Windows.Forms.TextBox();
            this.labelContent = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonAddFront
            // 
            this.buttonAddFront.Location = new System.Drawing.Point(340, 43);
            this.buttonAddFront.Name = "buttonAddFront";
            this.buttonAddFront.Size = new System.Drawing.Size(108, 53);
            this.buttonAddFront.TabIndex = 0;
            this.buttonAddFront.Text = "Dodaj na przod";
            this.buttonAddFront.UseVisualStyleBackColor = true;
            this.buttonAddFront.Click += new System.EventHandler(this.buttonAddFront_Click);
            // 
            // buttonAddEnd
            // 
            this.buttonAddEnd.Location = new System.Drawing.Point(340, 122);
            this.buttonAddEnd.Name = "buttonAddEnd";
            this.buttonAddEnd.Size = new System.Drawing.Size(108, 53);
            this.buttonAddEnd.TabIndex = 2;
            this.buttonAddEnd.Text = "Dodaj na koniec";
            this.buttonAddEnd.UseVisualStyleBackColor = true;
            this.buttonAddEnd.Click += new System.EventHandler(this.buttonAddEnd_Click);
            // 
            // buttonAddAtIndex
            // 
            this.buttonAddAtIndex.Location = new System.Drawing.Point(332, 194);
            this.buttonAddAtIndex.Name = "buttonAddAtIndex";
            this.buttonAddAtIndex.Size = new System.Drawing.Size(116, 69);
            this.buttonAddAtIndex.TabIndex = 3;
            this.buttonAddAtIndex.Text = "Dodaj na określoną pozycję";
            this.buttonAddAtIndex.UseVisualStyleBackColor = true;
            this.buttonAddAtIndex.Click += new System.EventHandler(this.buttonAddAtIndex_Click);
            // 
            // buttonDeleteFirst
            // 
            this.buttonDeleteFirst.Location = new System.Drawing.Point(513, 41);
            this.buttonDeleteFirst.Name = "buttonDeleteFirst";
            this.buttonDeleteFirst.Size = new System.Drawing.Size(108, 53);
            this.buttonDeleteFirst.TabIndex = 4;
            this.buttonDeleteFirst.Text = "Usuń pierwszy";
            this.buttonDeleteFirst.UseVisualStyleBackColor = true;
            this.buttonDeleteFirst.Click += new System.EventHandler(this.buttonDeleteFirst_Click);
            // 
            // buttonDeleteback
            // 
            this.buttonDeleteback.Location = new System.Drawing.Point(513, 122);
            this.buttonDeleteback.Name = "buttonDeleteback";
            this.buttonDeleteback.Size = new System.Drawing.Size(108, 53);
            this.buttonDeleteback.TabIndex = 5;
            this.buttonDeleteback.Text = "Usuń ostatni";
            this.buttonDeleteback.UseVisualStyleBackColor = true;
            this.buttonDeleteback.Click += new System.EventHandler(this.buttonDeleteback_Click);
            // 
            // buttonDeleteAtIndex
            // 
            this.buttonDeleteAtIndex.Location = new System.Drawing.Point(501, 194);
            this.buttonDeleteAtIndex.Name = "buttonDeleteAtIndex";
            this.buttonDeleteAtIndex.Size = new System.Drawing.Size(120, 61);
            this.buttonDeleteAtIndex.TabIndex = 6;
            this.buttonDeleteAtIndex.Text = "Usuń z określonej pozycji";
            this.buttonDeleteAtIndex.UseVisualStyleBackColor = true;
            this.buttonDeleteAtIndex.Click += new System.EventHandler(this.buttonDeleteAtIndex_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(332, 291);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(108, 53);
            this.buttonSearch.TabIndex = 7;
            this.buttonSearch.Text = "Szukaj";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Index";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Wartość";
            // 
            // textBoxIndex
            // 
            this.textBoxIndex.Location = new System.Drawing.Point(70, 153);
            this.textBoxIndex.Name = "textBoxIndex";
            this.textBoxIndex.Size = new System.Drawing.Size(100, 22);
            this.textBoxIndex.TabIndex = 9;
            this.textBoxIndex.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxIndex_KeyPress);
            // 
            // textBoxValue
            // 
            this.textBoxValue.Location = new System.Drawing.Point(70, 72);
            this.textBoxValue.Name = "textBoxValue";
            this.textBoxValue.Size = new System.Drawing.Size(100, 22);
            this.textBoxValue.TabIndex = 8;
            this.textBoxValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxValue_KeyPress);
            // 
            // labelContent
            // 
            this.labelContent.AutoSize = true;
            this.labelContent.Location = new System.Drawing.Point(98, 377);
            this.labelContent.Name = "labelContent";
            this.labelContent.Size = new System.Drawing.Size(0, 17);
            this.labelContent.TabIndex = 12;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(564, 377);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(99, 34);
            this.buttonSave.TabIndex = 13;
            this.buttonSave.Text = "Zapisz";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.labelContent);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxIndex);
            this.Controls.Add(this.textBoxValue);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.buttonDeleteAtIndex);
            this.Controls.Add(this.buttonDeleteback);
            this.Controls.Add(this.buttonDeleteFirst);
            this.Controls.Add(this.buttonAddAtIndex);
            this.Controls.Add(this.buttonAddEnd);
            this.Controls.Add(this.buttonAddFront);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAddFront;
        private System.Windows.Forms.Button buttonAddEnd;
        private System.Windows.Forms.Button buttonAddAtIndex;
        private System.Windows.Forms.Button buttonDeleteFirst;
        private System.Windows.Forms.Button buttonDeleteback;
        private System.Windows.Forms.Button buttonDeleteAtIndex;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxIndex;
        private System.Windows.Forms.TextBox textBoxValue;
        private System.Windows.Forms.Label labelContent;
        private System.Windows.Forms.Button buttonSave;
    }
}