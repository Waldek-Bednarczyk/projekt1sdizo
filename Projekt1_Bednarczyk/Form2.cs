﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt1_Bednarczyk
{
    public partial class Form2 : Form
    {
        private int[] tempData;
        private int[] values;
        Array myArray;
        int a = 30000;

        public Form2(int[] data)
        {
            InitializeComponent();
            tempData = data;
            myArray = new Array(tempData);
            string path = "C:\\Users\\walde\\Desktop\\data" + a + ".txt";
            using (StreamReader sr = new StreamReader(path))
            {
                string numbers = sr.ReadToEnd();
                string[] numbersSplitted = numbers.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);
                int numberOfValues = Convert.ToInt32(numbersSplitted[0]);
                values = new int[numberOfValues];
                for (int i = 0; i < numberOfValues; i++)
                {
                    values[i] = Convert.ToInt32(numbersSplitted[i + 1]);
                }

            }
        }

        private string Display()
        {
            string valuesToDisplay ="";
            for(int i=0; i<myArray.MyArray.Length; i++)
            {
                valuesToDisplay +=  myArray.MyArray[i].ToString() + " ";
            }
            return valuesToDisplay;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i=0; i< a; i++)
            {
                myArray.addBeginning(values[i]);
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonAddEnd_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i = 0; i < a; i++)
            {
                myArray.addEnd(values[i]);
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();

        }

        private void buttonAddAtIndex_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i=0; i<a; i++)
            {
                myArray.addAtIndex(values[i], i + 1);
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void textBoxValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && (e.KeyChar != '-');
        }

        private void textBoxIndex_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void buttonCheckIfExist_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i=0; i<a; i++)
            {
                myArray.checkIfExists(values[values.Length - i-1]);
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonDeleteFirst_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i=0; i<a; i++)
            {
                myArray.deleteFirst();
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonDeleteLast_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i = 0; i < a; i++)
            {
                myArray.deleteLast();
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonDeleteAtIndex_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i=0; i<a-11; i++)
            {
                myArray.deleteAtIndex(10);
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "Tablica.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(myArray.MyArray.Length + " " + Display());
            sw.Close();
        }
    }
}
