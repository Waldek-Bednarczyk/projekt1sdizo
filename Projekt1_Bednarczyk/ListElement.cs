﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Bednarczyk
{
    class ListElement
    {
        public ListElement Next { get; set; }
        public ListElement Prev { get; set; }
        public int Number { get; set; }

        public ListElement(int number)
        {
            Next = Prev = null;
            Number = number;
        }
    }
}
