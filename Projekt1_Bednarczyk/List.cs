﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Bednarczyk
{

    class List
    {
        public ListElement Front { get; set; }
        public ListElement Back { get; set; }
        public int counter { get; set; }

        public List()
        {
            Front = Back = null;
            counter = 0;
        } 

        public int numberOfElements()
        {
            return counter;
        }

        public string displayList()
        {
            string values = "";
            ListElement tmpFront = Front;
            for(int i=0; i<counter; i++)
            {
                values += tmpFront.Number + " ";
                tmpFront = tmpFront.Next;
            }
            return values;
        }

        public bool checkIfExists(int value)
        {
            ListElement tmpFront = Front;
            for(int i = 0; i<counter; i++)
            {
                if (tmpFront.Number == value) return true;
                tmpFront = tmpFront.Next;
            }
            return false;
        }

        public void AddFront(int value)
        {
            ListElement listElement = new ListElement(value);
            if(Front == null)
            {
                Front = listElement;
                Back = Front;
            }
            else
            {
                listElement.Next = Front;
                Front.Prev = listElement;
                Front = listElement;
            }
            counter++;
        }

        public void AddBack(int value)
        {
            ListElement listElement = new ListElement(value);
            if(Front == null)
            {
                Front = listElement;
                Back = Front;
            }
            else
            {
                listElement.Prev = Back;
                listElement.Prev.Next = listElement;
                Back = listElement;
            }
            counter++;
        }
        public void AddAtIndex(int value, int index)
        {
            if (index == 0)
            {
                AddFront(value);
                return;
            }
            if (index == counter)
            {
                AddBack(value);
                return;
            }
            ListElement listElement = new ListElement(value);
            if (index >= (counter / 2))
            {
                ListElement tmpBack = Back;
                for (int i = 0; i < (counter-index-1); i++)
                {
                    tmpBack = tmpBack.Prev;
                }
                listElement.Next = tmpBack;
                listElement.Prev = tmpBack.Prev;
                listElement.Prev.Next = listElement;
                tmpBack.Prev = listElement;
                counter++;
            }
            else
            {
                ListElement tmpFront = Front;
                for (int i = 0; i < index; i++)
                {
                    tmpFront = tmpFront.Next;
                }
                listElement.Next = tmpFront;
                listElement.Prev = tmpFront.Prev;
                listElement.Prev.Next = listElement;
                tmpFront.Prev = listElement;
                counter++;
            }      
        }

        public void deleteFront()
        {
            if (Front == null) return;
            else
            {   
                Front = Front.Next;
            }
            counter--;
        }

        public void deleteBack()
        {
            if (Front == null) return;
            else
            {
                Back = Back.Prev;
            }
            counter--;
        }

        public void deleteAtIndex(int index )
        {
            if (index == 0)
            {
                deleteFront();
                return;
            }
            if (index == counter - 1)
            {
                deleteBack();
                return;
            }
            if (index >= (counter / 2))
            {
                ListElement tmpBack = Back;
                for (int i = 0; i < (counter - index - 2); i++)
                {
                    tmpBack = tmpBack.Prev;
                }
                tmpBack.Prev = tmpBack.Prev.Prev;
                tmpBack.Prev.Next = tmpBack;
                counter--;
            }
            else
            {
                ListElement tmpFront = Front;
                for (int i = 0; i < index-1; i++)
                {
                    tmpFront = tmpFront.Next;
                }
                tmpFront.Next = tmpFront.Next.Next;
                tmpFront.Next.Prev = tmpFront;
                counter--;
            }
        }
    }
}