﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt1_Bednarczyk
{
    public partial class Form4 : Form
    {
        Heap heap;
        int a = 50000;
        private int[] values;

        public Form4(int[] data)
        {
            InitializeComponent();
            heap = new Heap();
            for (int i=0; i<data.Length; i++)
            {
                heap.push(data[i]);
            }
            //labelContent.Text = heap.display();
            string path = "C:\\Users\\walde\\Desktop\\data" + a + ".txt";
            using (StreamReader sr = new StreamReader(path))
            {
                string numbers = sr.ReadToEnd();
                string[] numbersSplitted = numbers.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);
                int numberOfValues = Convert.ToInt32(numbersSplitted[0]);
                values = new int[numberOfValues];
                for (int i = 0; i < numberOfValues; i++)
                {
                    values[i] = Convert.ToInt32(numbersSplitted[i + 1]);
                }

            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
            for (int i=0; i<a; i++)
                {
                    heap.push(values[i]);
                }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();
           
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();

            for (int i = 0; i < a-1; i++)
            {
                heap.pop();
            }
            stopwatch.Stop();
            time = stopwatch.ElapsedMilliseconds.ToString();

            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            var stopwatch = new Stopwatch();
            string time = "";
            stopwatch.Start();
 
                for (int i = 0; i < a; i++)
                {
                    heap.checkIfExists(values[values.Length-i-1]);
                }
           stopwatch.Stop();
          time = stopwatch.ElapsedMilliseconds.ToString();
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "dane.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(time);
            sw.Close();

        }

        private void textBoxValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && (e.KeyChar != '-');
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            var desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string path = Path.Combine(desktopFolder, "Kopiec.txt");
            StreamWriter sw = new StreamWriter(path);
            sw.Write(heap.counter + " " + heap.write());
            sw.Close();
        }
    }
}
