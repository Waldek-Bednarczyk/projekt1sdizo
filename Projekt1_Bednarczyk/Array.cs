﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Bednarczyk
{
    class Array
    {
        public int[] MyArray { get; set; }

        public Array(int[] data)
        {
            MyArray = data;
        }

        public bool checkIfExists(int value)
        {
            for(int i=0; i< MyArray.Length; i++)
            {
                if (MyArray[i] == value) return true;
            }
            return false;
        }

        public void addEnd(int value)
        {
            int[] tempArray = new int[MyArray.Length+1];
            for(int i=0; i< MyArray.Length; i++)
            {
                tempArray[i] = MyArray[i];
            }
            tempArray[MyArray.Length] = value;
            MyArray = tempArray;
        }

        public void addBeginning(int value)
        {
            int[] tempArray = new int[MyArray.Length + 1];
            for (int i = MyArray.Length; i > 0; i--)
            {
                tempArray[i] = MyArray[i-1];
            }
            tempArray[0] = value;
            MyArray = tempArray;
        }

        public void addAtIndex(int value, int index)
        {
            int[] tempArray = new int[MyArray.Length + 1];
            for(int i=0; i<index; i++)
            {
                tempArray[i] = MyArray[i];
            }
            tempArray[index] = value;
            for (int i = index+1; i < tempArray.Length; i++)
            {
                tempArray[i] = MyArray[i-1];
            }
            MyArray = tempArray;
        }

        public void deleteFirst()
        {
            if (MyArray.Length == 0) return;
            else
            {
                int[] tempArray = new int[MyArray.Length - 1];
                for (int i = 1; i < MyArray.Length; i++)
                {
                    tempArray[i - 1] = MyArray[i];
                }
                MyArray = tempArray;
            }
 
        }

        public void deleteLast()
        {
            if (MyArray.Length == 0) return;
            else
            {
                int[] tempArray = new int[MyArray.Length - 1];
                for (int i = 0; i < tempArray.Length; i++)
                {
                    tempArray[i] = MyArray[i];
                }
                MyArray = tempArray;
            }
        }

        public void deleteAtIndex(int index)
        {
            int[] tempArray = new int[MyArray.Length - 1];
            for (int i = 0; i < index; i++)
            {
                tempArray[i] = MyArray[i];
            }
            for(int i = index+1; i < MyArray.Length; i++)
            {
                tempArray[i-1] = MyArray[i];
            }
            MyArray = tempArray;
        }
    }
}
