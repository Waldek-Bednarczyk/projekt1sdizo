﻿namespace Projekt1_Bednarczyk
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonArray = new System.Windows.Forms.Button();
            this.buttonList = new System.Windows.Forms.Button();
            this.buttonHeap = new System.Windows.Forms.Button();
            this.buttonReadData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonArray
            // 
            this.buttonArray.Location = new System.Drawing.Point(284, 88);
            this.buttonArray.Name = "buttonArray";
            this.buttonArray.Size = new System.Drawing.Size(112, 53);
            this.buttonArray.TabIndex = 0;
            this.buttonArray.Text = "Tablica";
            this.buttonArray.UseVisualStyleBackColor = true;
            this.buttonArray.Click += new System.EventHandler(this.buttonArray_Click);
            // 
            // buttonList
            // 
            this.buttonList.Location = new System.Drawing.Point(284, 147);
            this.buttonList.Name = "buttonList";
            this.buttonList.Size = new System.Drawing.Size(112, 50);
            this.buttonList.TabIndex = 1;
            this.buttonList.Text = "Lista";
            this.buttonList.UseVisualStyleBackColor = true;
            this.buttonList.Click += new System.EventHandler(this.buttonList_Click);
            // 
            // buttonHeap
            // 
            this.buttonHeap.Location = new System.Drawing.Point(284, 203);
            this.buttonHeap.Name = "buttonHeap";
            this.buttonHeap.Size = new System.Drawing.Size(112, 52);
            this.buttonHeap.TabIndex = 2;
            this.buttonHeap.Text = "Kopiec";
            this.buttonHeap.UseVisualStyleBackColor = true;
            this.buttonHeap.Click += new System.EventHandler(this.buttonHeap_Click);
            // 
            // buttonReadData
            // 
            this.buttonReadData.Location = new System.Drawing.Point(242, 311);
            this.buttonReadData.Name = "buttonReadData";
            this.buttonReadData.Size = new System.Drawing.Size(75, 23);
            this.buttonReadData.TabIndex = 3;
            this.buttonReadData.Text = "Wczytaj dane";
            this.buttonReadData.UseVisualStyleBackColor = true;
            this.buttonReadData.Click += new System.EventHandler(this.buttonReadData_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 481);
            this.Controls.Add(this.buttonReadData);
            this.Controls.Add(this.buttonHeap);
            this.Controls.Add(this.buttonList);
            this.Controls.Add(this.buttonArray);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonArray;
        private System.Windows.Forms.Button buttonList;
        private System.Windows.Forms.Button buttonHeap;
        private System.Windows.Forms.Button buttonReadData;
    }
}

