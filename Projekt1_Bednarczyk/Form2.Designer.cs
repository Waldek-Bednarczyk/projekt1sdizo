﻿namespace Projekt1_Bednarczyk
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddBeginning = new System.Windows.Forms.Button();
            this.textBoxValue = new System.Windows.Forms.TextBox();
            this.buttonAddEnd = new System.Windows.Forms.Button();
            this.buttonAddAtIndex = new System.Windows.Forms.Button();
            this.textBoxIndex = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonCheckIfExist = new System.Windows.Forms.Button();
            this.buttonDeleteFirst = new System.Windows.Forms.Button();
            this.buttonDeleteLast = new System.Windows.Forms.Button();
            this.buttonDeleteAtIndex = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.labelContent = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonAddBeginning
            // 
            this.buttonAddBeginning.Location = new System.Drawing.Point(197, 23);
            this.buttonAddBeginning.Name = "buttonAddBeginning";
            this.buttonAddBeginning.Size = new System.Drawing.Size(144, 45);
            this.buttonAddBeginning.TabIndex = 0;
            this.buttonAddBeginning.Text = "Dodaj na początek";
            this.buttonAddBeginning.UseVisualStyleBackColor = true;
            this.buttonAddBeginning.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // textBoxValue
            // 
            this.textBoxValue.Location = new System.Drawing.Point(75, 56);
            this.textBoxValue.Name = "textBoxValue";
            this.textBoxValue.Size = new System.Drawing.Size(100, 22);
            this.textBoxValue.TabIndex = 1;
            this.textBoxValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxValue_KeyPress);
            // 
            // buttonAddEnd
            // 
            this.buttonAddEnd.Location = new System.Drawing.Point(197, 93);
            this.buttonAddEnd.Name = "buttonAddEnd";
            this.buttonAddEnd.Size = new System.Drawing.Size(144, 45);
            this.buttonAddEnd.TabIndex = 2;
            this.buttonAddEnd.Text = "Dodaj na koniec";
            this.buttonAddEnd.UseVisualStyleBackColor = true;
            this.buttonAddEnd.Click += new System.EventHandler(this.buttonAddEnd_Click);
            // 
            // buttonAddAtIndex
            // 
            this.buttonAddAtIndex.Location = new System.Drawing.Point(197, 156);
            this.buttonAddAtIndex.Name = "buttonAddAtIndex";
            this.buttonAddAtIndex.Size = new System.Drawing.Size(144, 45);
            this.buttonAddAtIndex.TabIndex = 3;
            this.buttonAddAtIndex.Text = "Dodaj na określoną pozycję";
            this.buttonAddAtIndex.UseVisualStyleBackColor = true;
            this.buttonAddAtIndex.Click += new System.EventHandler(this.buttonAddAtIndex_Click);
            // 
            // textBoxIndex
            // 
            this.textBoxIndex.Location = new System.Drawing.Point(75, 137);
            this.textBoxIndex.Name = "textBoxIndex";
            this.textBoxIndex.Size = new System.Drawing.Size(100, 22);
            this.textBoxIndex.TabIndex = 4;
            this.textBoxIndex.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxIndex_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Wartość";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Index";
            // 
            // buttonCheckIfExist
            // 
            this.buttonCheckIfExist.Location = new System.Drawing.Point(197, 218);
            this.buttonCheckIfExist.Name = "buttonCheckIfExist";
            this.buttonCheckIfExist.Size = new System.Drawing.Size(144, 45);
            this.buttonCheckIfExist.TabIndex = 7;
            this.buttonCheckIfExist.Text = "Szukaj";
            this.buttonCheckIfExist.UseVisualStyleBackColor = true;
            this.buttonCheckIfExist.Click += new System.EventHandler(this.buttonCheckIfExist_Click);
            // 
            // buttonDeleteFirst
            // 
            this.buttonDeleteFirst.Location = new System.Drawing.Point(395, 23);
            this.buttonDeleteFirst.Name = "buttonDeleteFirst";
            this.buttonDeleteFirst.Size = new System.Drawing.Size(121, 45);
            this.buttonDeleteFirst.TabIndex = 8;
            this.buttonDeleteFirst.Text = "Usuń pierwszy";
            this.buttonDeleteFirst.UseVisualStyleBackColor = true;
            this.buttonDeleteFirst.Click += new System.EventHandler(this.buttonDeleteFirst_Click);
            // 
            // buttonDeleteLast
            // 
            this.buttonDeleteLast.Location = new System.Drawing.Point(395, 93);
            this.buttonDeleteLast.Name = "buttonDeleteLast";
            this.buttonDeleteLast.Size = new System.Drawing.Size(121, 39);
            this.buttonDeleteLast.TabIndex = 9;
            this.buttonDeleteLast.Text = "Usuń ostatni";
            this.buttonDeleteLast.UseVisualStyleBackColor = true;
            this.buttonDeleteLast.Click += new System.EventHandler(this.buttonDeleteLast_Click);
            // 
            // buttonDeleteAtIndex
            // 
            this.buttonDeleteAtIndex.Location = new System.Drawing.Point(395, 156);
            this.buttonDeleteAtIndex.Name = "buttonDeleteAtIndex";
            this.buttonDeleteAtIndex.Size = new System.Drawing.Size(121, 45);
            this.buttonDeleteAtIndex.TabIndex = 10;
            this.buttonDeleteAtIndex.Text = "Usuń z określonej pozycji";
            this.buttonDeleteAtIndex.UseVisualStyleBackColor = true;
            this.buttonDeleteAtIndex.Click += new System.EventHandler(this.buttonDeleteAtIndex_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(395, 257);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(99, 34);
            this.buttonSave.TabIndex = 14;
            this.buttonSave.Text = "Zapisz";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelContent
            // 
            this.labelContent.AutoSize = true;
            this.labelContent.Location = new System.Drawing.Point(88, 330);
            this.labelContent.Name = "labelContent";
            this.labelContent.Size = new System.Drawing.Size(0, 17);
            this.labelContent.TabIndex = 15;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 433);
            this.Controls.Add(this.labelContent);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonDeleteAtIndex);
            this.Controls.Add(this.buttonDeleteLast);
            this.Controls.Add(this.buttonDeleteFirst);
            this.Controls.Add(this.buttonCheckIfExist);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxIndex);
            this.Controls.Add(this.buttonAddAtIndex);
            this.Controls.Add(this.buttonAddEnd);
            this.Controls.Add(this.textBoxValue);
            this.Controls.Add(this.buttonAddBeginning);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAddBeginning;
        private System.Windows.Forms.TextBox textBoxValue;
        private System.Windows.Forms.Button buttonAddEnd;
        private System.Windows.Forms.Button buttonAddAtIndex;
        private System.Windows.Forms.TextBox textBoxIndex;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonCheckIfExist;
        private System.Windows.Forms.Button buttonDeleteFirst;
        private System.Windows.Forms.Button buttonDeleteLast;
        private System.Windows.Forms.Button buttonDeleteAtIndex;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label labelContent;
    }
}