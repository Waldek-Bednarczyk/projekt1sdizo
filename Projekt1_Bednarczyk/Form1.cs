﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt1_Bednarczyk
{
    public partial class Form1 : Form
    {
        int[] values;

        public Form1()
        {
            InitializeComponent();
        }

        private bool CheckIfEmpty()
        {
            if (values == null || values.Length == 0)
            {
                MessageBox.Show("Brak danych \nDalsze operacje nie mają sensu", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void buttonReadData_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt";
            string path="";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                path = openFileDialog.FileName;
            }
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string numbers = sr.ReadToEnd();
                    string[] numbersSplitted = numbers.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);
                    int numberOfValues = Convert.ToInt32(numbersSplitted[0]);
                    values = new int[numberOfValues];
                    for(int i=0; i < numberOfValues; i++)
                    {
                        values[i] = Convert.ToInt32(numbersSplitted[i+1]);
                    }
                    if (numberOfValues != numbersSplitted.Length-1)
                    {
                        MessageBox.Show("Ilość liczb nie jest zgodna", "Ostrzeżenie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    MessageBox.Show("Wczytywanie danych zakończone powodzeniem", "Powodzenie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Nie udało się wczytać danych", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonArray_Click(object sender, EventArgs e)
        {
            if(CheckIfEmpty())
            {
                Form2 frm2 = new Form2(values);
                frm2.Show();
            }
        }

        private void buttonList_Click(object sender, EventArgs e)
        {
            if (CheckIfEmpty())
            {
                Form3 frm3 = new Form3(values);
                frm3.Show();
            }
        }

        private void buttonHeap_Click(object sender, EventArgs e)
        {
            if (CheckIfEmpty())
            {
                Form4 frm4 = new Form4(values);
                frm4.Show();
            }
        }
    }
}
