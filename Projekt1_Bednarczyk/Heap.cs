﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Bednarczyk
{
    class Heap
    {
        public int[] MyHeap { get; set; }
        public int counter { get; set; }

        public Heap()
        {
            counter = 0;
        }

        public string display()
        {
            string display = "";
            int j=1,k;
            for(int i=0; i< counter;i++)
            {
                display += i + ".poziom: ";
                k = j;
                for(; k<j*2; k++)
                {
                    if (k >= counter) return display;
                    display += MyHeap[k-1] + " ";
                }
                j *= 2;
                display += "\n";
            }
            return display;
        }

        public string write()
        {
            string valuesToDisplay = "";
            for (int i = 0; i < counter; i++)
            {
                valuesToDisplay += MyHeap[i].ToString() + " ";
            }
            return valuesToDisplay;
        }

        public void push(int value)
        {
            int[] tempHeap = new int[counter + 1];
            for (int k=0; k<counter; k++)
            {
                tempHeap[k] = MyHeap[k];
            }
            tempHeap[counter] = value;

            int i = counter;
            int j = 0;

            while (j!=1)
            {
                if (tempHeap[(i - 1) / 2] < tempHeap[i])
                {
                    int temp = tempHeap[i];
                    tempHeap[i] = tempHeap[(i - 1) / 2];
                    tempHeap[(i - 1) / 2] = temp;
                    i = (i - 1) / 2;
                }
                else j = 1;
            }
            MyHeap = tempHeap;
            counter++;
        }

        public void pop()
        {
            if (counter-- != 0)
            {
                int[] tempHeap = new int[counter];
                for (int k = 1; k < (counter); k++)
                {
                    tempHeap[k] = MyHeap[k];
                }
                tempHeap[0] = MyHeap[counter];
                int i = 0, j = 1;

                while (j < counter)
                {
                    if (counter > j + 1 && tempHeap[j + 1] > tempHeap[j]) j++;
                    if (tempHeap[i] >= tempHeap[j]) break;
                    int tmp = tempHeap[i];
                    tempHeap[i] = tempHeap[j];
                    tempHeap[j] = tmp;
                    i = j;
                    j = 2 * j + 1;
                }
                MyHeap = tempHeap;
            }
        }

        public bool checkIfExists(int value)
        {
            for (int i=0; i<counter; i++)
            {
                if (MyHeap[i] == value) return true;
            }
            return false;
        }
    }
}
